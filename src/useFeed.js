import { useEffect, useState } from "react";
import axios from "axios";

function useFeed(url) {
  const [response, setResponse] = useState(null);
  const [loading, setLoading] = useState(true);
  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    (async () => {
      setLoading(true);
      axios
        .get(url)
        .then(res => {
          setResponse(res.data);
        })
        .finally(() => {
          setLoading(false);
       })
        .catch(() => {
          setHasError(true);
        });
    })();
  }, [url]);

  return [response, loading, hasError];
}
export default useFeed;
