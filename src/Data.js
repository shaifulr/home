export const homeObjOne = {
  id: null,
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: null,
  headline: null,
  description:
    null,
  imgStart: false,
  img: require("./images/svg-1.svg"),
  alt: "Me",
  dark: true,
  primary: true,
  darkText: false
};
export const homeObjTwo = {
  id: "interest",
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: "Likes",
  headline: "My personal interests which includes",
  description:
    "Internet security, technology, nature, aquascape, gaming and even teaching ESL.",
  buttonLabel: "Read more",
  imgStart: false,
  img: require("./images/svg-3.svg"),
  alt: "Me",
  dark: true,
  primary: true,
  darkText: false
};
export const homeObjThree = {
  id: null,
  lightBg: true,
  lightText: false,
  lightTextDesc: false,
  topLine: null,
  headline: null,
  description:
    null,
  buttonLabel: null,
  imgStart: true,
  img: require("./images/svg-5.svg"),
  alt: "community",
  dark: false,
  primary: false,
  darkText: true
};

export const homeObjFour = {
  id: null,
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: null,
  headline: null,
  description:
    null,
  buttonLabel: null,
  imgStart: false,
  img: require("./images/svg-6.svg"),
  alt: "pnc",
  dark: true,
  primary: true,
  darkText: false
};