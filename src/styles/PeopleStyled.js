import styled from "styled-components";
import { Link } from "react-router-dom";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  line-height: 20px;

  background: linear-gradient(
    108deg,
    rgba(1, 147, 86, 1) 0%,
    rgba(10, 201, 122, 1) 100%
  );
`;

export const HeaderWrap = styled.div`
  min-height: 68px;
  width: 80%;
  margin-top: 8px;
  display: flex;
  justify-content: space-between;
  align-items: baseline;

  @media screen and (max-width: 480px) {
    margin-left: 16px;
    margin-top: 8px;
  }
`;

export const Icon = styled(Link)`
  margin-left: 32px;
  margin-top: 16px;
  text-decoration: none;
  color: #fff;
  font-weight: 700;
  font-size: 32px;
  position: relative;

  &::after {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 3.1rem;
    height: 3.1rem;
    background: linear-gradient(135deg, #0e5cad 10%, #79f1a4 100%);
    border-radius: 50%;
    transform: translate(-15%, -30%);
    mix-blend-mode: color-burn;
  }
`;

export const SearchInput = styled.input`
  height: 48px;
  width: 40%;
  padding: 16px 16px;
  margin-left: 12px;
  margin-bottom: 12px;
  border: none;
  border-radius: 4px;
  background-image: linear-gradient(to right, #a1ffce, #ffffff);
  color: #141414;
`;

export const FormH1 = styled.h1`
  margin-bottom: 20px;
  color: #fff;
  font-size: 20px;
  font-weight: 400;
  text-align: center;
`;

export const ContentWrap = styled.div`
  box-sizing: border-box;
  display: block;
  position: relative;
  height: calc(100vh - 77px);
  overflow: hidden;

  @media screen and (max-width: 480px) {
    display: flex;
    flex-direction: column;
  }
`;

export const MapOuterWrap = styled.div`
  position: absolute;
  height: 94%;
  width: 62%;
  left: 0;
  top: 0;
  bottom: 0;

  @media screen and (max-width: 480px) {
    position: relative;
    display: flex;
    width: 90%;
    height: 50%;
  }
`;

export const MapWrapper = styled.div`
  height: 100%;
  width: 100%;
  margin: 0;
  padding: 0;
  min-height: 280px;
  min-width: 200px;
`;

export const FilterList = styled.div`
  position: absolute;
  height: 100%;
  width: 38%;
  right: 0;
  top: 0;
  overflow: auto;

  @media screen and (max-width: 480px) {
    display: flex;
    flex-direction: column;
    width: 100%;
    top: 50%;
  }
`;

export const FilterListInner = styled.div`
  margin-bottom: 32px;
`;

export const FilterListHeader = styled.div`
  height: 20%;
  border-top: 1px solid #e0e0e0;
  border-bottom: 1px solid #e0e0e0;
  position: relative;
  padding-left: 10px;
  font-size: 14px;
  color: #fff;

  @media screen and (max-width: 480px) {
    display: none;
  }
`;

export const FilterHr = styled.div`
  padding-left: 10px;
  padding-right: 20px;
  margin-bottom: 10px;

  hr {
    border: none;
    border-bottom: solid 1px #e0e0e0;
    margin-top: 16px;
    color: #fff;
  }
`;

export const FilterHref = styled.div`
position: -webkit-sticky;
  position: sticky;
  top: 0;

  a {
    margin-top: 22px;
    cursor: pointer;
    font-size: 12px;
    text-decoration: none;
    background-color: transparent;
    color: #fff;
  }
`;

export const FilterH1 = styled.h1`
  height: 30px;
  width: 30px;
  margin-right: 10px;
`;

export const TableContainer = styled.div`
  width: 100%;

  @media screen and (max-width: 480px) {
    display: flex;
    justify-content: center;
  }
`;

export const TableWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;

  @media screen and (max-width: 480px) {
    width: 100%;
  }
`;

export const TableRowWrapper = styled.div`
  display: flex;
  width: 468px;
  height: 125px;
  background: #fff;
  float: none;

  &:hover {
    background: #e0e0e0;
  }

  @media screen and (max-width: 480px) {
    width: 100%;
  }
`;

export const TableColumn1 = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 25%;
`;

export const TableColumn2 = styled.div`
  padding-left: 32px;
  font-size: 13px;
  width: 100%;
`;

export const TableColumn3 = styled.div`
  padding: 0;
  width: 25%;
`;

export const ColumnImg = styled.img`
  height: 100%;
  width: 100%;
`;

export const ColumnNameH1 = styled.div`
  font-size: 16px;
  width: 150px;
`;
export const ColumnStreetP = styled.p`
  text-transform: uppercase;
`;

export const ColumnPhoneP = styled.p`
  width: 110px;
`;

export const ColumnSocmedIcons = styled.p`
  width: 240px;
`;

export const ColumnModelP = styled.p`
  width: 80px;
`;

export const ColumnCompanyP = styled.p`
  width: 200px;
`;
