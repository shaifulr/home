import styled from "styled-components";
import { MdClose } from "react-icons/md";
import { Link } from "react-router-dom";

export const Container = styled.div`
  min-height: 692px;
  position: relative;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  z-index: 0;
  overflow: hidden;
  background: linear-gradient(
    108deg,
    rgba(1, 147, 86, 1) 0%,
    rgba(10, 201, 122, 1) 100%
  );
`;

export const FormWrap = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  
  @media screen and (max-width: 480px) {
    height: 80%;
  }
`;

export const Icon = styled(Link)`
  margin-left: 32px;
  margin-top: 32px;
  text-decoration: none;
  color: #fff;
  font-weight: 700;
  font-size: 32px;

  @media screen and (max-width: 480px) {
    margin-left: 16px;
    margin-top: 8px;
  }
`;

export const FormContent = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-content: center;

  @media screen and (max-width: 480px) {
    padding: 10px;
  }
`;

export const Form = styled.form`
  background: #fff;
  max-width: 800px;
  width: 100%;
  display: grid;
  margin: 0 auto;
  padding: 80px 32px;
  border-radius: 4px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.9);

  @media screen and (max-width: 400px) {
    height: 80%;
    padding: 32px 32px;
  }
`;

export const FormH1 = styled.h1`
  margin-bottom: 20px;
  color: #141414;
  font-size: 20px;
  font-weight: 400;
  text-align: center;
`;

export const FormLabel = styled.label`
  margin-bottom: 8px;
  font-size: 14px;
  color: #141414;
`;

export const FormInput = styled.input`
  padding: 16px 16px;
  margin-bottom: 12px;
  border: none;
  border-radius: 4px;
`;

export const FormInput2 = styled.textarea`
  padding: 16px 16px;
  margin-bottom: 12px;
  box-sizing: border-box;
  border: none;
  border-radius: 4px;
  resize: none;
`;

export const FormButton = styled.button`
  background: #01bf71;
  padding: 16px 0;
  border: none;
  border-radus: 4px;
  color: #fff;
  font-size: 20px;
  cursor: pointer;
`;

export const Text = styled.span`
  text-allign: center;
  margin-top: 24px;
  color: #141414;
  font-size: 14px;
`;

export const Background = styled.div`
  width: 100%;
  height: 100%;
  z-index: 10;
  background: rgba(0, 0, 0, 0.8);
  position: fixed;
  top: 0;
  left: 0;
  overflow: auto;
  display: flex;
  justify-content: center;
  allign-items: center;
`;

export const ModalWrapper = styled.div`
  width: 800px;
  max-width: calc(100% - 80px);
  box-shadow: 0 5px 16px rgba(0, 0, 0, 0.2);
  background: #fff;
  color: #000;
  top: 20px;
  right: -20px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  position: relative;
  z-index: 5;
  border-radius: 10px;

  @media screen and (max-width: 768px) {
    grid-template-columns: auto;
    margin: 15% auto;
    width: 70%;
    height: 75%;
  }

  @media screen and (max-width: 580px) {
    top: 40px;
    right: 30px;
    margin: -6% auto;
    width: 90%
    height: 80%;
  }
`;

export const ModalImg = styled.img`
  width: 100%;
  height: 100%;
  border-radius: 10px 0 0 10px;
  background: #000;

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  line-height: 1.8;
  color: #141414;

  button {
    padding: 10px 24px;
    background: #141414;
    color: #fff;
    border: none;
  }
`;

export const CloseModalButton = styled(MdClose)`
  cursor: pointer;
  position: absolute;
  top: 20px;
  right: 20px;
  width: 32px;
  height: 32px;
  padding: 0;
  z-index: 10;

  @media screen and (max-width: 580px) {
    right: -60px;
  }
`;
