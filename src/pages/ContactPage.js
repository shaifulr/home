import React, { Fragment } from "react";
import {
  Container,
  FormWrap,
  Icon,
  FormContent,
  Form,
  FormH1,
  FormLabel,
  FormInput,
  FormButton,
  Text
} from "../styles/ContactStyled";

const Contact = () => {
  return (
    <Fragment>
      <Container>
        <FormWrap>
          <Icon to="/">Tzorp</Icon>
          <FormContent>
            <Form action="#">
              <FormH1>Contact Tzorp</FormH1>
              <FormLabel htmlFor="for">Message</FormLabel>
              <FormInput type="text" required />
              <FormLabel htmlFor="for">Email</FormLabel>
              <FormInput type="email" required />
              <FormButton type="submit">Continue</FormButton>
              <Text>GMail</Text>
            </Form>
          </FormContent>
        </FormWrap>
      </Container>
    </Fragment>
  );
};

export default Contact;
