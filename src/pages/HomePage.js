import React, { Fragment, useState, useEffect } from "react";
import Navbar from "../components/Navbar";
import Sidebar from "../components/Sidebar";
import ContactModal from "../components/ContactModal";
import Splash from "../components/Splash";
import Infos from "../components/Infos";
import { homeObjOne, homeObjTwo, homeObjThree, homeObjFour } from "../Data";
import Projects from "../components/Projects";
import Footer from "../components/Footer";
import useFeed from "../useFeed";

const Home = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };

  const [showModal, setShowModal] = useState(false);

  const openModal = () => {
    setShowModal(prev => !prev);
  };

  const [response, loading ,hasError] = useFeed("https://shaifulr.000webhostapp.com/wp-json/wp/v2/infos");

  useEffect(() => {
    if (response !== null) {
      response.forEach(info => {
        if (info.slug === "about") {
          homeObjOne.id = info.slug;
          homeObjOne.topLine = info.acf.topline;
          homeObjOne.headline = info.acf.headline;
          homeObjOne.description = info.acf.description;
          homeObjOne.buttonLabel = info.acf.button_label;
        } else if (info.slug === "people") {
          homeObjThree.id = info.slug;
          homeObjThree.topLine = info.acf.topline;
          homeObjThree.headline = info.acf.headline;
          homeObjThree.description = info.acf.description;
          homeObjThree.buttonLabel = info.acf.button_label;
          homeObjThree.buttonLabel2 = info.acf.button_label2;
        } else if (info.slug === "services") {
          homeObjFour.id = info.slug;
          homeObjFour.topLine = info.acf.topline;
          homeObjFour.headline = info.acf.headline;
          homeObjFour.description = info.acf.description;
          homeObjFour.buttonLabel = info.acf.button_label;
        }
      });
    } else {
      if (hasError) console.log("Error: ", hasError);
    }
  }, [response, loading, hasError]);

  return (
    <Fragment>
      <Sidebar isOpen={isOpen} toggle={toggle} />
      <Navbar toggle={toggle} />
      <ContactModal showModal={showModal} setShowModal={setShowModal} />
      <Splash />
      <Infos pageL="interest" {...homeObjOne} />
      <Projects />
      <Infos pageL="home" {...homeObjTwo} />
      <Infos pageL="/people" openModal={openModal} {...homeObjThree} />
      <Infos openModal={openModal} {...homeObjFour} />
      <Footer />
    </Fragment>
  );
};

export default Home;
