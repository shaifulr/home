import React, { Fragment, useState, useEffect } from "react";
import Map from "google-map-react";
import useFeed from "../useFeed";
import {
  Container,
  HeaderWrap,
  Icon,
  SearchInput,
  FormH1,
  MapOuterWrap,
  MapWrapper,
  ContentWrap,
  FilterList,
  FilterListInner,
  FilterListHeader,
  FilterHr,
  FilterHref,
  FilterH1
} from "../styles/PeopleStyled";
import People from "../components/PeopleTable";
import Marker from "../components/Marker";
import {
  PageFooterWrap,
  SocialIcons,
  SocialIconLink,
  WebsiteRights,
  TermsLink
} from "../styles/FooterStyled";
import {
  FaFacebook,
  FaInstagram,
  FaYoutube,
  FaTwitter,
  FaLinkedin
} from "react-icons/fa";

function PeoplePage() {
  const [people, setPeople] = useState([]);
  const [search, setSearch] = useState("");
  const [hover, setHover] = useState({}.center);

  const [response, loading, hasError] = useFeed(
    "https://jsonplaceholder.typicode.com/users"
  );

  useEffect(() => {
    if (response !== null) {
      setPeople(response);
    } else {
      if (hasError) console.log("Error: ", hasError);
    }
  }, [response, hasError]);

  const handleChange = e => {
    setSearch(e.target.value);
  };

  const filteredPeople = people.filter(pe =>
    pe.name.toLowerCase().includes(search.toLowerCase())
  );

  const defaultMap = {
    center: {
      lat: 2.975956,
      lng: 107.281468
    },
    styles: {
      width: "100%",
      height: "100%"
    },
    zoom: 5
  };

  const handleHover = (lat, lng) => {
    setHover({ lat: parseFloat(lat), lng: parseFloat(lng) });
  };

  return (
    <Fragment>
      <Container>
        <HeaderWrap>
          <Icon to="/">Tzorp</Icon>
          <SearchInput
            type="text"
            placeholder="Search"
            onChange={handleChange}
          />
          <FormH1></FormH1>
        </HeaderWrap>
        <ContentWrap>
          <MapOuterWrap>
            <MapWrapper>
              <Map
                bootstrapURLKeys={{
                  key: "AIzaSyCGrzCDVciaw7L68lUkWx6b9s8PnzvORSo"
                }}
                defaultCenter={defaultMap.center}
                defaultZoom={defaultMap.zoom}
                style={defaultMap.styles}
                center={hover}
              >
                {people.map(pe => (
                  <Marker
                    key={pe.id}
                    text={pe.company.name}
                    lat={pe.address.geo.lat}
                    lng={pe.address.geo.lng}
                  />
                ))}
              </Map>
            </MapWrapper>
          </MapOuterWrap>
          <FilterList>
            <FilterListHeader>
              Filters
              <FilterHr>
                <hr /><hr /><hr />
              </FilterHr>
              <FilterHref><a href="/">Back to top</a></FilterHref>
            </FilterListHeader>
            <FilterListInner>
              {loading ? (
                <FilterH1>Loading list..</FilterH1>
              ) : (
                filteredPeople.map(pe => {
                  return (
                    <People
                      handleHover={handleHover}
                      key={pe.id}
                      name={pe.name}
                      imageHeader={require("../images/svg-11.svg")}
                      street={pe.address.street}
                      phone={pe.phone}
                      socmed={pe.website}
                      bModel="FnB"
                      companyName={pe.company.name}
                      lat={pe.address.geo.lat}
                      lng={pe.address.geo.lng}
                    />
                  );
                })
              )}
            </FilterListInner>
          </FilterList>
        </ContentWrap>
        <PageFooterWrap>
          <SocialIcons>
            <SocialIconLink href="/" target="_blank" aria-label="Facebook">
              <FaFacebook />
            </SocialIconLink>
            <SocialIconLink href="/" target="_blank" aria-label="Instagram">
              <FaInstagram />
            </SocialIconLink>
            <SocialIconLink href="/" target="_blank" aria-label="Twitter">
              <FaTwitter />
            </SocialIconLink>
            <SocialIconLink href="/" target="_blank" aria-label="Youtube">
              <FaYoutube />
            </SocialIconLink>
            <SocialIconLink href="/" target="_blank" aria-label="LinkedIn">
              <FaLinkedin />
            </SocialIconLink>
          </SocialIcons>
          <WebsiteRights>
            Tzorp © {new Date().getFullYear()} All rights reserved.
          </WebsiteRights>
          <TermsLink href="/" target="_blank">Terms of Services</TermsLink>
        </PageFooterWrap>
      </Container>
    </Fragment>
  );
}

export default PeoplePage;
