import "./App.css";
import React, { Component, Fragment } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./pages/HomePage";
import Contact from "./pages/ContactPage";
import People from "./pages/PeoplePage";

class App extends Component {

  render() {
    return (
      <Router>
        <Fragment>
          <Route exact path="/" component={Home} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/people" component={People} />
        </Fragment>
      </Router>
    );
  }
}

export default App;
