import { useState, useEffect } from "react";

const useForm = (callback) => {
  const [values, setValues] = useState({
    message: "",
    email: ""
  });
  const [errors] = useState({});

  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleChange = e => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value
    });
  };

  const handleSubmit = e => {
    e.preventDefault();

    setIsSubmitting(true);
  };

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      callback();
    }
  }, [isSubmitting, callback, errors]);

  return { handleChange, values, handleSubmit, errors };
};

export default useForm;
