import React, { Fragment } from "react";
import { Button, ButtonR, ButtonL } from "../styles/ButtonStyled";

import {
  InfoContainer,
  InfoWrapper,
  InfoRow,
  Column1,
  Column2,
  TextWrapper,
  TopLine,
  Heading,
  Subtitle,
  BtnWrap,
  ImgWrap,
  Img
} from "../styles/InfosStyled";

const Infos = ({
  openModal,
  pageL,
  lightBg,
  id,
  imgStart,
  topLine,
  lightText,
  headline,
  darkText,
  description,
  buttonLabel,
  buttonLabel2,
  img,
  alt,
  primary,
  dark,
  dark2
}) => {
  return (
    <Fragment>
      <InfoContainer lightBg={lightBg} id={id}>
        <InfoWrapper>
          <InfoRow imgStart={imgStart}>
            <Column1>
              <TextWrapper>
                <TopLine>{topLine}</TopLine>
                <Heading lightText={lightText}>{headline}</Heading>
                <Subtitle darkText={darkText}>{description}</Subtitle>
                <BtnWrap>
                  {!pageL ? (
                    <Button
                      onClick={openModal}
                      primary={primary ? 1 : 0}
                      dark={dark ? 1 : 0}
                      dark2={dark2 ? 1 : 0}
                    >
                      {buttonLabel}
                    </Button>
                  ) : id === "people" ? (
                    <Button
                      onClick={openModal}
                      primary={primary ? 1 : 0}
                      dark={dark ? 1 : 0}
                      dark2={dark2 ? 1 : 0}
                    >
                      {buttonLabel}
                    </Button>
                  ) : (
                    <ButtonL
                      to={pageL}
                      smooth={true}
                      duration={500}
                      spy={true}
                      exact="true"
                      offset={50}
                      primary={primary ? 1 : 0}
                      dark={dark ? 1 : 0}
                      dark2={dark2 ? 1 : 0}
                    >
                      {buttonLabel}
                    </ButtonL>
                  )}
                  {id === "people" ? (
                    <ButtonR
                      to={pageL}
                      exact="true"
                      primary="true"
                      dark="true"
                      dark2="true"
                    >
                      {buttonLabel2}
                    </ButtonR>
                  ) : null}
                </BtnWrap>
              </TextWrapper>
            </Column1>
            <Column2>
              <ImgWrap>
                <Img src={img} alt={alt} />
              </ImgWrap>
            </Column2>
          </InfoRow>
        </InfoWrapper>
      </InfoContainer>
    </Fragment>
  );
};

export default Infos;
