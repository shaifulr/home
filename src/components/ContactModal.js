import React, {
  useState,
  useRef,
  useEffect,
  useCallback,
  Fragment
} from "react";
import {
  Background,
  ModalWrapper,
  ModalImg,
  ModalContent,
  CloseModalButton
} from "../styles/ContactStyled";
import { useSpring, animated } from "react-spring";
import HireForm from "./HireForm";
import SuccessForm from "./SuccessForm";

const ContactModel = ({ showModal, setShowModal, pageL }) => {
  const modalRef = useRef();

  const animation = useSpring({
    config: {
      duration: 250
    },
    opacity: showModal ? 1 : 0,
    transform: showModal ? `translateY(0%)` : `translateY(-100%)`
  });

  const closeModal = e => {
    if (modalRef.current === e.target) {
      setShowModal(false);
    }
  };

  const keyPress = useCallback(
    e => {
      if (e.key === "Escape" && showModal) {
        setShowModal(false);
      }
    },
    [setShowModal, showModal]
  );

  useEffect(() => {
    document.addEventListener("keydown", keyPress);
    return () => document.removeEventListener("keydown", keyPress);
  }, [keyPress]);

  const [isSubmitted, setIsSubmitted] = useState(false);

  function submitForm() {
    setIsSubmitted(true);
  }

  return (
    <Fragment>
      {showModal ? (
        <Background ref={modalRef} onClick={closeModal}>
          <animated.div style={animation}>
            <ModalWrapper showModal={showModal}>
              {isSubmitted ? (
                <ModalImg
                  src={require("../images/svg-10.svg")}
                  alt="modal img"
                />
              ) : pageL === "people" ? null : (
                <ModalImg
                  src={require("../images/svg-8.svg")}
                  alt="modal img"
                />
              )}
              <ModalContent>
                {!isSubmitted ? (
                  <HireForm submitForm={submitForm} />
                ) : (
                  <SuccessForm />
                )}
              </ModalContent>
              <CloseModalButton
                aria-label="Close modal"
                onClick={() => setShowModal(prev => !prev)}
              />
            </ModalWrapper>
          </animated.div>
        </Background>
      ) : null}
    </Fragment>
  );
};

export default ContactModel;
