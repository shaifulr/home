import React, { Fragment, useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { FaBars } from "react-icons/fa";
import { IconContext } from "react-icons/lib";
import { animateScroll as scroll } from "react-scroll";
import {
  Nav,
  NavbarContainer,
  NavLogo,
  MobileIcon,
  NavMenu,
  NavItem,
  NavLinks,
  NavBtn,
  NavBtnLink
} from "../styles/NavbarStyled";

const Navbar = ({ toggle }) => {
  const [scrollNav, setScrollNav] = useState(false);

  const location = useLocation();

  useEffect(() => {
    if (location.pathname === "/") {
      const changeNav = () => {
        if (window.scrollY >= 80) {
          setScrollNav(true);
        } else {
          setScrollNav(false);
        }
      };

      window.addEventListener("scroll", changeNav, true);

      return () => {
        window.removeEventListener("scroll", changeNav, true);
      };
    }
  }, [location.pathname]);

  const toggleHome = () => {
    scroll.scrollToTop();
  };

  return (
    <Fragment>
      <IconContext.Provider value={{ color: "#fff" }}>
        <Nav scrollNav={scrollNav}>
          <NavbarContainer>
            <NavLogo to="/" onClick={toggleHome}>
              Tzorp
            </NavLogo>
            <MobileIcon onClick={toggle}>
              <FaBars />
            </MobileIcon>
            <NavMenu>
              <NavItem>
                <NavLinks
                  to="about"
                  smooth={true}
                  duration={500}
                  spy={true}
                  exact="true"
                  offset={50}
                >
                  About
                </NavLinks>
              </NavItem>
              <NavItem>
                <NavLinks
                  to="projects"
                  smooth={true}
                  duration={500}
                  spy={true}
                  exact="true"
                  offset={50}
                >
                  Projects
                </NavLinks>
              </NavItem>
              <NavItem>
                <NavLinks
                  to="interest"
                  smooth={true}
                  duration={500}
                  spy={true}
                  exact="true"
                  offset={50}
                >
                  Interest
                </NavLinks>
              </NavItem>
              <NavItem>
                <NavLinks
                  to="people"
                  smooth={true}
                  duration={500}
                  spy={true}
                  exact="true"
                  offset={50}
                >
                  People
                </NavLinks>
              </NavItem>
              <NavItem>
                <NavLinks
                  to="services"
                  smooth={true}
                  duration={500}
                  spy={true}
                  exact="true"
                  offset={50}
                >
                  Services
                </NavLinks>
              </NavItem>
            </NavMenu>
            <NavBtn>
              <NavBtnLink to="/contact">Contact</NavBtnLink>
            </NavBtn>
          </NavbarContainer>
        </Nav>
      </IconContext.Provider>
    </Fragment>
  );
};

export default Navbar;
