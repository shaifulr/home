import React, { Fragment } from "react";
import Icon1 from '../images/svg-7.svg';
import Icon2 from '../images/svg-4.svg';
import Icon3 from '../images/svg-6.svg';
import {
  ProjectsContainer,
  ProjectsWrapper,
  ProjectsCard,
  ProjectsIcon,
  ProjectsH1,
  ProjectsH2,
  ProjectsP
} from "../styles/ProjectsStyled";

const Projects = () => {
  return (
    <Fragment>
      <ProjectsContainer id="projects">
        <ProjectsH1>Our Projects</ProjectsH1>
        <ProjectsWrapper>
          <ProjectsCard>
            <ProjectsIcon src={Icon1} />
            <ProjectsH2>Blackberry SeqStats</ProjectsH2>
            <ProjectsP>
              Blackberry OS mobile applicaton to fetch and display amino acid
              statistics of a DNA or RNA sequence.
            </ProjectsP>
          </ProjectsCard>
          <ProjectsCard>
            <ProjectsIcon src={Icon2} />
            <ProjectsH2>BioD Crawler</ProjectsH2>
            <ProjectsP>
              A Drupal PHP module created to fetch and display biodiversity data
              from web services portal via an API.
            </ProjectsP>
          </ProjectsCard>
          <ProjectsCard>
            <ProjectsIcon src={Icon3} />
            <ProjectsH2>Blood Ordering System</ProjectsH2>
            <ProjectsP>
              A comprehensive web system which facilitates the order and
              transfer of blood bags from blood center to hospitals.
            </ProjectsP>
          </ProjectsCard>
        </ProjectsWrapper>
      </ProjectsContainer>
    </Fragment>
  );
};

export default Projects;
