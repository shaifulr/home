import React from "react";

const SuccessForm = () => {
  return (
    <div className="form-content-right">
      <div className="form-success">We have received your message!</div>
    </div>
  );
};

export default SuccessForm;
