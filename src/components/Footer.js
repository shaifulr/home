import React, { Fragment } from "react";
import { animateScroll as scroll } from "react-scroll";
import {
  FooterContainer,
  FooterWrap,
  FooterLinksContainer,
  FooterLinksWrapper,
  FooterLinkItems,
  FooterLinkTitle,
  FooterLink,
  SocialMedia,
  SocialMediaWrap,
  SocialLogo,
  WebsiteRights,
  SocialIcons,
  SocialIconLink
} from "../styles/FooterStyled";
import {
  FaFacebook,
  FaInstagram,
  FaYoutube,
  FaTwitter,
  FaLinkedin
} from "react-icons/fa";

const Footer = () => {

  const toggleHome = () => {
    scroll.scrollToTop();
  };

  return (
    <Fragment>
      <FooterContainer id="footer">
        <FooterWrap>
          <FooterLinksContainer>
            <FooterLinksWrapper>
              <FooterLinkItems>
                <FooterLinkTitle>About Us</FooterLinkTitle>
                <FooterLink to="/contact">Hire us</FooterLink>
                <FooterLink to="/contact">How we work</FooterLink>
                <FooterLink to="/contact">Testimonials</FooterLink>
                <FooterLink to="/contact">Contact</FooterLink>
                <FooterLink to="/contact">Terms of Services</FooterLink>
              </FooterLinkItems>
              <FooterLinkItems>
                <FooterLinkTitle>About Us</FooterLinkTitle>
                <FooterLink to="/contact">Hire us</FooterLink>
                <FooterLink to="/contact">How we work</FooterLink>
                <FooterLink to="/contact">Testimonials</FooterLink>
                <FooterLink to="/contact">Contact</FooterLink>
                <FooterLink to="/contact">Terms of Services</FooterLink>
              </FooterLinkItems>
            </FooterLinksWrapper>
            <FooterLinksWrapper>
              <FooterLinkItems>
                <FooterLinkTitle>About Us</FooterLinkTitle>
                <FooterLink to="/contact">Hire us</FooterLink>
                <FooterLink to="/contact">How we work</FooterLink>
                <FooterLink to="/contact">Testimonials</FooterLink>
                <FooterLink to="/contact">Contact</FooterLink>
                <FooterLink to="/contact">Terms of Services</FooterLink>
              </FooterLinkItems>
              <FooterLinkItems>
                <FooterLinkTitle>About Us</FooterLinkTitle>
                <FooterLink to="/contact">Hire us</FooterLink>
                <FooterLink to="/contact">How we work</FooterLink>
                <FooterLink to="/contact">Testimonials</FooterLink>
                <FooterLink to="/contact">Contact</FooterLink>
                <FooterLink to="/contact">Terms of Services</FooterLink>
              </FooterLinkItems>
            </FooterLinksWrapper>
          </FooterLinksContainer>
          <SocialMedia>
            <SocialMediaWrap>
              <SocialLogo to="/" onClick={toggleHome}>Tzorp</SocialLogo>
              <WebsiteRights>
                Tzorp © {new Date().getFullYear()} All rights reserved.
              </WebsiteRights>
              <SocialIcons>
                <SocialIconLink href="/" target="_blank" aria-label="Facebook">
                  <FaFacebook />
                </SocialIconLink>
                <SocialIconLink href="/" target="_blank" aria-label="Instagram">
                  <FaInstagram />
                </SocialIconLink>
                <SocialIconLink href="/" target="_blank" aria-label="Twitter">
                  <FaTwitter />
                </SocialIconLink>
                <SocialIconLink href="/" target="_blank" aria-label="Youtube">
                  <FaYoutube />
                </SocialIconLink>
                <SocialIconLink href="/" target="_blank" aria-label="LinkedIn">
                  <FaLinkedin />
                </SocialIconLink>
              </SocialIcons>
            </SocialMediaWrap>
          </SocialMedia>
        </FooterWrap>
      </FooterContainer>
    </Fragment>
  );
};

export default Footer;
