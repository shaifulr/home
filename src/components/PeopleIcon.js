import React from 'react'
import {
    FaHamburger
  } from "react-icons/fa";

const PeopleIcon = () => {
    return (
        <FaHamburger />
    )
}

export default PeopleIcon
