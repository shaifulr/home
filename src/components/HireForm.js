import React from "react";
import useForm from "../useForm";
import {
  Form,
  FormH1,
  FormLabel,
  FormInput,
  FormInput2,
  FormButton,
  Text
} from "../styles/ContactStyled";

const HireForm = ({ submitForm }) => {
  const { handleChange, values, handleSubmit } = useForm(submitForm);
  return (
    <Form action="#" onSubmit={handleSubmit}>
      <FormH1>Hire Tzorp</FormH1>
      <FormLabel htmlFor="for">Project outline</FormLabel>
      <FormInput2
        type="textarea"
        name="message"
        value={values.message}
        onChange={handleChange}
        required
      />
      <FormLabel htmlFor="for">Email</FormLabel>
      <FormInput
        type="email"
        name="email"
        value={values.email}
        onChange={handleChange}
        required
      />
      <FormButton type="submit">Continue</FormButton>
      <Text>We will reply to the email provided promptly.</Text>
    </Form>
  );
};

export default HireForm;
