import React, { Fragment, useState } from "react";
import { ButtonL } from "../styles/ButtonStyled";
import {
  HeroContainer,
  HeroBg,
  VideoBg,
  HeroContent,
  HeroH1,
  HeroP,
  HeroBtnWrapper,
  ArrowForward,
  ArrowRight
} from "../styles/SplashStyled";
import Video from "../videos/video.mp4";

const Splash = () => {
  const [hover, setHover] = useState(false);

  const onHover = () => {
    setHover(!hover);
  };
  return (
    <Fragment>
      <HeroContainer id="home">
        <HeroBg>
          <VideoBg autoPlay loop muted src={Video} type="video/mp4" />
        </HeroBg>
        <HeroContent>
          <HeroH1>Tzorp Technology</HeroH1>
          <HeroP>Connecting SME to the world.</HeroP>
          <HeroBtnWrapper>
            <ButtonL
              to="services"
              smooth={true}
              duration={500}
              spy={true}
              exact="true"
              offset={50}
              onMouseEnter={onHover}
              onMouseLeave={onHover}
              primary="true"
              dark="true"
            >
              Get expertise {hover ? <ArrowForward /> : <ArrowRight />}
            </ButtonL>
          </HeroBtnWrapper>
        </HeroContent>
      </HeroContainer>
    </Fragment>
  );
};

export default Splash;
