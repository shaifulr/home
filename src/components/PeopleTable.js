import React, { Fragment } from "react";
import PeopleIcon from "./PeopleIcon";
import {
  TableContainer,
  TableWrapper,
  TableRowWrapper,
  TableColumn1,
  TableColumn2,
  TableColumn3,
  ColumnImg,
  ColumnNameH1,
  ColumnStreetP,
  ColumnPhoneP,
  ColumnCompanyP
} from "../styles/PeopleStyled";

const People = ({
  handleHover,
  name,
  imageHeader,
  street,
  phone,
  bModel,
  companyName,
  lat,
  lng
}) => {
  return (
    <Fragment>
      <TableContainer>
        <TableWrapper>
          <TableRowWrapper>
            <TableColumn1>
              <PeopleIcon bModel={bModel} />
            </TableColumn1>
            <TableColumn2>
              <ColumnNameH1>{name}</ColumnNameH1>
              <ColumnCompanyP>{companyName}</ColumnCompanyP>
              <ColumnStreetP>{street}</ColumnStreetP>
              <ColumnPhoneP>+6 {phone}</ColumnPhoneP>
            </TableColumn2>
            <TableColumn3>
              <ColumnImg
                src={imageHeader}
                onClick={() => handleHover(lat, lng)}
                alt="profile pic"
              />
            </TableColumn3>
          </TableRowWrapper>
        </TableWrapper>
      </TableContainer>
    </Fragment>
  );
};

export default People;
